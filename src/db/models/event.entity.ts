import { Field, ObjectType } from '@nestjs/graphql';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import User from './user.entity';
import Guest from './guest.entity';

@ObjectType()
@Entity({ name: 'events' })
export default class Event {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ name: 'user_id' })
  userId: number;

  @Field()
  @Column()
  title: string;

  @Field()
  @Column()
  description: string;

  @Field()
  @Column({ name: 'start_at' })
  startAt: string;

  @Field()
  @Column({ name: 'end_at' })
  endAt: string;

  @Field()
  @Column()
  tz: string;

  @Field()
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Field()
  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @Field(() => User)
  user: User;

  @ManyToOne(() => User, (user) => user.eventConnection, { primary: true })
  @JoinColumn({ name: 'user_id' })
  userConnection: Promise<User>;

  @OneToMany(() => Guest, (guest) => guest.eventConnection)
  guestConnection: Guest[];

  @Field()
  isGuest?: boolean;
}
