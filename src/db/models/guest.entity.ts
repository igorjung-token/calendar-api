import { Field, ObjectType } from '@nestjs/graphql';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import User from './user.entity';
import Event from './event.entity';

@ObjectType()
@Entity({ name: 'guests' })
export default class Guest {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ name: 'user_id' })
  userId: number;

  @Field()
  @Column({ name: 'event_id' })
  eventId: number;

  @Field()
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Field()
  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @Field(() => User)
  user: User;

  @ManyToOne(() => User, (user) => user.guestConnection, { primary: true })
  @JoinColumn({ name: 'user_id' })
  userConnection: User;

  @ManyToOne(() => Event, (event) => event.guestConnection, { primary: true })
  @JoinColumn({ name: 'event_id' })
  eventConnection: Promise<Event>;
}
