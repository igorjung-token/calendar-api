import { Field, InputType } from '@nestjs/graphql';

@InputType()
export default class GuestInput {
  @Field()
  readonly userId: number;

  @Field()
  readonly eventId: number;
}
