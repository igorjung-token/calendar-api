import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';

import RepoService from '../repo/repo.service';
import Guest from '../db/models/guest.entity';
import GuestInput from './input/guest.input';
import User from 'src/db/models/user.entity';
import Event from 'src/db/models/event.entity';

@Resolver(() => Guest)
export default class GuestResolver {
  constructor(private readonly repoService: RepoService) {}

  @Query(() => [Guest])
  public async getGuests(): Promise<Guest[]> {
    return this.repoService.guestRepo.find();
  }

  @Query(() => [Guest])
  public async getGuestsFromEvent(
    @Args('eventId') eventId: number,
  ): Promise<Guest[]> {
    return this.repoService.guestRepo.find({
      where: { eventId },
    });
  }

  @Query(() => Guest, { nullable: true })
  public async getGuest(@Args('id') id: number): Promise<Guest> {
    return this.repoService.guestRepo.findOne(id);
  }

  @Mutation(() => Guest)
  public async createGuest(@Args('data') input: GuestInput): Promise<Guest> {
    const guest = this.repoService.guestRepo.create({
      userId: input.userId,
      eventId: input.eventId,
    });

    return this.repoService.guestRepo.save(guest);
  }

  @ResolveField(() => User, { name: 'user' })
  public async getUser(@Parent() parent: Guest): Promise<User> {
    return this.repoService.userRepo.findOne(parent.userId);
  }

  @ResolveField(() => Event, { name: 'event' })
  public async getEvent(@Parent() parent: Guest): Promise<Event> {
    return this.repoService.eventRepo.findOne(parent.eventId);
  }
}
