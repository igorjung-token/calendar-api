import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

import RepoService from '../repo/repo.service';
import User from '../db/models/user.entity';
import UserInput from './input/user.input';

@Resolver(() => User)
export default class UserResolver {
  constructor(private readonly repoService: RepoService) {}

  @Query(() => [User])
  public async getUsers(): Promise<User[]> {
    return this.repoService.userRepo.find();
  }

  @Query(() => User, { nullable: true })
  public async getUser(@Args('id') id: number): Promise<User> {
    return this.repoService.userRepo.findOne(id);
  }

  @Mutation(() => User)
  public async createUser(@Args('data') input: UserInput): Promise<User> {
    const password = await bcrypt.hash(input.password, 10);
    const user = this.repoService.userRepo.create({
      email: input.email.toLocaleLowerCase().trim(),
      password,
    });

    await this.repoService.userRepo.save(user);
    const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);

    return {
      ...user,
      token,
    };
  }

  @Mutation(() => User)
  public async login(@Args('data') input: UserInput): Promise<User> {
    const user = await this.repoService.userRepo.findOne({
      where: { email: input.email },
    });

    if (!user) {
      throw new Error(`User ${input.email} not found`);
    }

    const valid = await bcrypt.compare(input.password, user.password);

    if (!valid) {
      throw new Error(`Validation error!`);
    }

    const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);

    return {
      ...user,
      token,
    };
  }
}
