import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as dotenv from 'dotenv';

import * as ormOptions from './config/orm';

import RepoModule from './repo/repo.module';
import UserResolver from './resolvers/user.resolver';
import EventResolver from './resolvers/event.resolver';
import GuestResolver from './resolvers/guest.resolver';

const gqlImports = [UserResolver, EventResolver, GuestResolver];

dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forRoot(ormOptions),
    RepoModule,
    ...gqlImports,
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      playground: true,
    }),
    ConfigModule,
  ],
})
export class AppModule {}
