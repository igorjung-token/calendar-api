import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import RepoService from './repo.service';
import User from '../db/models/user.entity';
import Event from '../db/models/event.entity';
import Guest from '../db/models/guest.entity';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([User, Event, Guest])],
  providers: [RepoService],
  exports: [RepoService],
})
class RepoModule {}
export default RepoModule;
