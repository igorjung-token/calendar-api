import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import User from '../db/models/user.entity';
import Event from '../db/models/event.entity';
import Guest from '../db/models/guest.entity';

@Injectable()
class RepoService {
  public constructor(
    @InjectRepository(User) public readonly userRepo: Repository<User>,
    @InjectRepository(Event) public readonly eventRepo: Repository<Event>,
    @InjectRepository(Guest) public readonly guestRepo: Repository<Guest>,
  ) {}
}

export default RepoService;
